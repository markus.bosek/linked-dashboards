# linked-dasbhoards

This extension enables grouping dashboards and passing filters via navigation.
The home page of the extension shows a list of all eligible folders.
Only folders with a prefix "LD-" are eligible for this extension.
In the Link column the linked-dashboards view of the folder is available.
The linked-dashboards view of a folder shows all dashboards in the navigation.
The first dashboard is automatically loaded.
Changing filters in the active dashboard updates the URL and the links in the navigation.
Updating the URL also allows for deep linking to the current view.

## Functions
- Toggle navigation bar
- Toggle embed mode
- Back to extension home
- Extension home
  - Load folders with *LD-* prefix
  - Search folders in data table
  - Click on link to open linked-dashboard view
- linked-dashboard view
  - Load all dashboards in navigation
  - Show selected dashboard in main view
  - Update URL on filter change event
  - Update navigation links on filter change event
- Support deep linking

## Limitations
- Developed and tested only for user-defined dashboards (lookml-dashboards not tested - might work, I don't know)
- Filters are passed via URL which means they must have the exact same name in all dashboards

## Code
The repository contains the Looker project code in the master branch and the extension code in the extension/react branch.
The extension code is based on https://cloud.google.com/looker/docs/components-example
Further helpful resources were found at:
- https://cloud.google.com/looker/docs/intro-to-extension-framework
- https://cloud.google.com/looker/docs/extension-framework-react-and-js-code-examples
- https://cloud.google.com/looker/docs/embedded-javascript-events
- https://cloud.google.com/looker/docs/components
